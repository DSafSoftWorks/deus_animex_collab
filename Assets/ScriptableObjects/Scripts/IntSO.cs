﻿using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/Int")]
public class IntSO : ScriptableObject
{
    public int value;
}
