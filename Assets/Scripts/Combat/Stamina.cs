﻿using UnityEngine;

public class Stamina : MonoBehaviour
{
    [SerializeField] private IntSO _max = default;
    [SerializeField] private IntSO _current = default;
    [SerializeField] private int recoveryPerSec = 4;
    [SerializeField] private float timeBeforeRecovery = 1;
    private float recoveryCounter;

    public int max
    {
        get => _max.value;
        set => _max.value = value;
    }
    public int current
    {
        get => _current.value;
        set
        {
            recoveryCounter = 0;
            _current.value = Clamp(value);
        }
    }
    
    protected int Clamp(int value) => (value > max) ? max : (value < 0) ? 0 : value;

    private void Update()
    {
        if (current < max)
        {
            if (recoveryCounter < timeBeforeRecovery)
            {
                recoveryCounter += Time.deltaTime;
            }
            else if (recoveryCounter < timeBeforeRecovery + 1)
            {
                    recoveryCounter += Time.deltaTime;
            }
            else
            {
                recoveryCounter = timeBeforeRecovery;
                _current.value += recoveryPerSec;
            }
        }
    }

    public void InhibitRecovery() => recoveryCounter = 0;
}
