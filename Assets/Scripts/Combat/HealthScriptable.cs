﻿using UnityEngine;

public class HealthScriptable : Health
{
    [SerializeField] private IntSO _max = default;
    [SerializeField] private IntSO _current = default;

    public override int max
    {
        get => _max.value;
        set => _max.value = value;
    }
    public override int current
    {
        get => _current.value;
        set
        {
            var initial = _current.value;
            _current.value = Clamp(value);
            CheckForHealthChange(initial);
        }
    }
}
