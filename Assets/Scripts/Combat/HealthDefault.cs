﻿using System;
using UnityEngine;

public class HealthDefault : Health
{
    [SerializeField] private int _max;
    public override int max
    {
        get => _max;
        set => _max = value;
    }
    [SerializeField] private int _current = default;
    public override int current
    {
        get => _current;
        set
        {
            var initial = _current;
            _current = Clamp(value);
            CheckForHealthChange(initial);
        }
    }
}

public abstract class Health : MonoBehaviour
{
    public event Action<int> OnHealthChange;

    public abstract int max { get; set; }
    public abstract int current { get; set; }

    protected int Clamp(int value) => (value > max) ? max : (value < 0) ? 0 : value;
    protected void CheckForHealthChange(int initial)
    {
        if (current != initial)
        {
            OnHealthChange?.Invoke(current - initial);
        }
    }
}
