﻿using UnityEngine;

public class Hitbox : MonoBehaviour
{
    // Relies on using Layer Collision Matrix under Project Settings > Physics2D
    [SerializeField] private int damage = 1;
    private IHittable parentHittable;

    private void Awake() => parentHittable = GetComponentInParent<IHittable>();

    private void OnTriggerEnter2D(Collider2D other)
    {
        var hit = other.GetComponentInParent<IHittable>();
        if (hit != null && hit != parentHittable)
        {
            var transform = GetComponentInParent<Rigidbody2D>()?.transform;
            if (transform == null)
            {
                transform = this.transform;
            }
            hit.OnHit(damage, transform);
        }
    }
}

public interface IHittable
{
    void OnHit(int damage, Transform other);
}
