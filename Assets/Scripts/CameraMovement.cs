﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform target = default;
    [SerializeField] private float smoothing = 0.1f;
    [SerializeField] private Vector2 maxPosition = default;
    [SerializeField] private Vector2 minPosition = default;

    void LateUpdate()
    {
        if(transform.position != target.position)
        {
            Vector3 newPosition = new Vector3(target.position.x, target.position.y, transform.position.z);
            newPosition.x = Mathf.Clamp(target.position.x, minPosition.x, maxPosition.x);
            newPosition.y = Mathf.Clamp(target.position.y, minPosition.y, maxPosition.y);
            transform.position = Vector3.Lerp(transform.position, newPosition, smoothing);
        }
    }
}
