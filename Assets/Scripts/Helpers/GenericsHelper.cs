﻿using System.Collections.Generic;

public static class GenericsHelper
{
    public static TValue Get<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
    {
        TValue result;
        dictionary.TryGetValue(key, out result);
        return result;
    }
}
