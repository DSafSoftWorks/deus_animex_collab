﻿using UnityEngine;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Stamina))]
public class Player : Actor, ICanTestAttack, ICanDash, ICanBlock, ICanParry, IHittable
{
    [Header("Attack variables")]
    [SerializeField] private int attackStaminaCost = 4;
    [SerializeField] private float _attackDuration = 0.65f;
    public float attackDuration => _attackDuration;
    [SerializeField] private float _attackThrust = 3.5f;
    public float attackThrust => _attackThrust;
    [SerializeField] private float _attackThrustDuration = 0.15f;
    public float attackThrustDuration => _attackThrustDuration;
    [SerializeField] private string attackAnimationBool = "Attacking";
    [SerializeField] private string attack2AnimationBool = "Attack2";
    [SerializeField] private Collider2D hitboxTEMP = default;
    public Collider2D hitbox => hitboxTEMP;

    [Header("Block variables")]
    [SerializeField] private int blockStaminaCost = 4;
    [SerializeField] private float _blockStrength = default;
    public float blockStrength => _blockStrength;
    [SerializeField] private float _blockMoveSpeedModifier = 1;
    public float blockMoveSpeedModifier => _blockMoveSpeedModifier;
    [SerializeField] [Range(0, 180)] private float _blockAngleMax = 45f;
    public float blockAngleMax => _blockAngleMax;

    [Header("Parry variables")]
    [SerializeField] [Range(0, 2)] private float _parryTimeWindow = 1f;
    public float parryTimeWindow => _parryTimeWindow;
    [SerializeField] [Range(0, 180)] private float _parryAngleMax = 45f;
    public float parryAngleMax => _parryAngleMax;

    [Header("Dash variables")]
    [SerializeField] private int dashStaminaCost = 8;
    [SerializeField] private bool _canDash = default;
    public bool canDash { get => _canDash; set => _canDash = value; }
    [SerializeField] private float _dashDistance = default;
    public float dashDistance => _dashDistance;
    [SerializeField] private float _dashDuration = default;
    public float dashDuration => _dashDuration;
    [SerializeField] private float _dashCooldown = default;
    public float dashCooldown => _dashCooldown;

    private Health health;
    private Stamina stamina;

    protected override void Awake()
    {
        base.Awake();

        health = GetComponent<Health>();
        stamina = GetComponent<Stamina>();

        allowExtendInput = true;
        animator.SetFloat("MoveX", 0);
        animator.SetFloat("MoveY", -1);
    }

    protected override void HandleInput()
    {
        directionInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        actionInput[ActionInput.Attack] = Input.GetButtonDown("Attack");
        actionInput[ActionInput.Dash] = Input.GetButtonDown("Dash");
        actionInput[ActionInput.Block] = Input.GetButton("Block");
        actionInput[ActionInput.Parry] = Input.GetButtonDown("Parry");
    }

    protected override void HandleState()
    {
        if (currentState is Dash || currentState is Parry) { return; }

        if (actionInput.Get(ActionInput.Attack) && stamina.current >= attackStaminaCost && !animator.GetBool(attack2AnimationBool))
        {
            stamina.current -= attackStaminaCost;
            if (currentState is TestAttack)
            {
                currentState = null;
                currentState = new TestAttack(this, this, attack2AnimationBool);
            }
            else
            {
                currentState = new TestAttack(this, this, attackAnimationBool);
            }
        }
        else if (!(currentState is TestAttack))
        {
            if (actionInput.Get(ActionInput.Dash) && canDash && stamina.current >= dashStaminaCost)
            {
                stamina.current -= dashStaminaCost;
                currentState = new Dash(this, this);
            }
            else if (actionInput.Get(ActionInput.Parry))
            {
                currentState = new Parry(this, this);
            }
            else if (actionInput.Get(ActionInput.Block) && stamina.current >= blockStaminaCost)
            {
                stamina.InhibitRecovery();
                currentState = new Block(this, this);
            }
            else if (directionInput != Vector2.zero)
            {
                currentState = new Walk(this);
            }
            else
            {
                currentState = null;
                rigidbody.velocity = Vector2.zero;
            }
        }
    }

    public void OnHit(int damage, Transform other)
    {
        if (currentState is Block)
        {
            var block = currentState as Block;
            stamina.current -= blockStaminaCost;
            damage = block.BlockDamage(damage, other);
        }
        else if (currentState is Parry)
        {
            var parry = currentState as Parry;
            if (parry.ParryCheck(other))
            {
                damage = 0;
                Debug.Log("Parry succeeded");
            }
        }

        health.current -= damage;
    }
}