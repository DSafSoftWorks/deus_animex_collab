﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionInput
{
    Attack,
    Dash,
    Block,
    Parry
}

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public abstract class Actor : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 4;
    public float moveSpeed => _moveSpeed;

    protected bool allowExtendInput;

    private Vector2 _directionInput;
    public Vector2 directionInput
    {
        get => _directionInput;
        protected set
        {
            if (value != Vector2.zero)
            {
                directionInputOld = _directionInput;
            }
            _directionInput = value;
        }
    }
    protected Vector2 directionInputOld;
    protected Dictionary<ActionInput, bool> actionInput { get; set; } = new Dictionary<ActionInput, bool>();

    private State _currentState;
    public State currentState {
        get => _currentState;
        set
        {
            if (value?.GetType() != _currentState?.GetType())
            {
                _currentState?.Exit();
                _currentState = value;
                _currentState?.Enter();
            }
        }
    }

    public Vector2 facingDirection => new Vector2(animator.GetFloat("MoveX"), animator.GetFloat("MoveY"));

    public new Rigidbody2D rigidbody {get; private set; }
    public Animator animator { get; private set; }

    protected virtual void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    protected abstract void HandleInput();
    protected abstract void HandleState();

    private void Update() {
        HandleInput();
        HandleState();
        currentState?.Update();

        if (allowExtendInput && (currentState is Walk || currentState == null))
        {
            CheckExtendAnimationInput();
        }
    }

    private void FixedUpdate()
    {
        currentState?.FixedUpdate();
    }

    public void SetAnimatorMoveParameters(Vector2 moveDirection) {
        animator.SetFloat("MoveX", Mathf.Round(moveDirection.x));
        animator.SetFloat("MoveY", Mathf.Round(moveDirection.y));
    }

    private void CheckExtendAnimationInput()
    {
        if (directionInput.x != directionInputOld.x && directionInput.x == 0)
        {
            StartCoroutine(ExtendAnimationInput("MoveX", directionInputOld.x));
        }
        if (directionInput.y != directionInputOld.y && directionInput.y == 0)
        {
            StartCoroutine(ExtendAnimationInput("MoveY", directionInputOld.y));
        }
    }

    private IEnumerator ExtendAnimationInput(string floatName, float value)
    {
        float extraTime = 0.05f;
        for (float i = 0; i < extraTime; i += Time.deltaTime)
        {
            animator.SetFloat(floatName, value);
            // Debug.Log("loop: " + directionInputOld.x + ", " + directionInputOld.y + ", " + value);
            yield return null;
        }
    }
}
