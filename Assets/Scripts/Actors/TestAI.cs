﻿using UnityEngine;

public class TestAI : Actor, IHittable, ICanTestAttack
{
    [SerializeField] private Transform target = default;
    [SerializeField] private float leeway = 0.75f;
    [Header("Attack variables")]
    [SerializeField] private float attackCooldown = 0f;
    private float attackCountdown;
    [SerializeField] private float _attackDuration = 0.65f;
    public float attackDuration => _attackDuration;
    [SerializeField] private float _attackThrust = 3.5f;
    public float attackThrust => _attackThrust;
    [SerializeField] private float _attackThrustDuration = 0.15f;
    public float attackThrustDuration => _attackThrustDuration;
    [SerializeField] private string attackAnimationBool = "Attacking";
    [SerializeField] private Collider2D hitboxTEMP = default;
    public Collider2D hitbox => hitboxTEMP;

    protected override void HandleInput()
    {
        if (target == null) { return; }
        var difference = target.position - transform.position;
        directionInput = difference.normalized;
    }

    protected override void HandleState()
    {
        if (currentState is TestAttack) { return; }

        var difference = target.position - transform.position;
        if (difference.magnitude > leeway)
        {
            currentState = new Walk(this);
        }
        else
        {
            if (attackCountdown > 0)
            {
                currentState = null;
                rigidbody.velocity = Vector2.zero;
                attackCountdown -= Time.deltaTime;
            }
            else
            {
                attackCountdown = attackCooldown + attackDuration;
                currentState = new TestAttack(this, this, attackAnimationBool);
            }
        }
    }

    public void OnHit(int damage, Transform other)
    {
        Debug.Log(gameObject.name + " was hit by " + other.gameObject.name + " for " + damage + " damage.");
    }
}
