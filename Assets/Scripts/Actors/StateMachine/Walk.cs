﻿using UnityEngine;

public class Walk : State
{
    public Walk(Actor actor)
    {
        this.actor = actor;
    }

    public override void Enter()
    {
        actor.animator.SetBool("Moving", true);
    }

    public override void Update()
    {
        actor.SetAnimatorMoveParameters(actor.directionInput);
    }

    public override void FixedUpdate()
    {
        actor.rigidbody.velocity = actor.directionInput.normalized * actor.moveSpeed;
        // actor.rigidbody.MovePosition(actor.transform.position
        //     + (Vector3)(actor.directionInput.normalized * actor.moveSpeed * Time.deltaTime));
    }

    public override void Exit()
    {
        actor.animator.SetBool("Moving", false);
        actor.rigidbody.velocity = Vector2.zero;
    }
}
