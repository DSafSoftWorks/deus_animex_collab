﻿using UnityEngine;

public class Parry : State
{
    private ICanParry parrier;
    private float timer;

    public Parry(Actor actor, ICanParry parrier)
    {
        this.actor = actor;
        this.parrier = parrier;

        timer = parrier.parryTimeWindow;
    }

    public override void Enter()
    {
        actor.animator.SetBool("Parrying", true);
        Debug.Log("Parry started");
    }

    public override void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            actor.currentState = null;
        }
    }

    public override void FixedUpdate()
    {
        // Prevent sliding if pushed.
        actor.rigidbody.velocity = Vector2.zero;
    }

    public override void Exit()
    {
        actor.animator.SetBool("Parrying", false);
        Debug.Log("Parry finished");
    }

    public bool ParryCheck(Transform other)
    {
        var diff = other.position - actor.transform.position;
        diff.Normalize();

        var angle = Vector2.Angle(actor.facingDirection.normalized, diff);
        if (angle <= parrier.parryAngleMax)
        {
            actor.currentState = null;
            return true;
        }
        return false;
    }
}

public interface ICanParry
{
    float parryTimeWindow { get; }
    float parryAngleMax { get; }
}
