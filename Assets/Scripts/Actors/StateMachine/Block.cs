﻿using UnityEngine;

public class Block : State
{
    private ICanBlock blocker;

    public Block(Actor actor, ICanBlock blocker)
    {
        this.actor = actor;
        this.blocker = blocker;
    }

    public override void Enter()
    {
        actor.animator.SetBool("Blocking", true);
    }

    public override void Update()
    {}

    public override void FixedUpdate()
    {
        actor.rigidbody.velocity = actor.directionInput.normalized * actor.moveSpeed * blocker.blockMoveSpeedModifier;
    }

    public override void Exit()
    {
        actor.animator.SetBool("Blocking", false);
        actor.rigidbody.velocity = Vector2.zero;
    }

    public int BlockDamage(int damage, Transform other)
    {
        var diff = other.position - actor.transform.position;
        diff.Normalize();

        var angle = Vector2.Angle(actor.facingDirection.normalized, diff);
        if (angle <= blocker.blockAngleMax)
        {
            // Do calculations for blocking damage.
            return 0;
        }
        else
        {
            return damage;
        }
    }
}

public interface ICanBlock
{
    float blockStrength { get; }
    float blockMoveSpeedModifier { get; }
    float blockAngleMax { get; }
}
