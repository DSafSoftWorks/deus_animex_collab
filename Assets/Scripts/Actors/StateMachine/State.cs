﻿public abstract class State
{
    protected Actor actor;

    public abstract void Enter();
    public abstract void Update();
    public abstract void FixedUpdate();
    public abstract void Exit();
}
