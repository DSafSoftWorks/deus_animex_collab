﻿using System.Collections;
using UnityEngine;

public class Dash : State
{
    private ICanDash dasher;
    private float timer;
    private Vector2 velocity;

    public Dash(Actor actor, ICanDash dasher)
    {
        this.actor = actor;
        this.dasher = dasher;

        timer = dasher.dashDuration;
        velocity = (dasher.dashDistance / dasher.dashDuration) * actor.facingDirection.normalized;
    }

    public override void Enter()
    {
        actor.animator.SetBool("Dashing", true);
        
        dasher.canDash = false;
    }

    public override void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            actor.currentState = null;
        }
    }

    public override void FixedUpdate()
    {
        actor.rigidbody.velocity = velocity;
    }

    public override void Exit()
    {
        actor.animator.SetBool("Dashing", false);
        actor.rigidbody.velocity = Vector2.zero;

        actor.StartCoroutine(DashCooldown());
    }

    private IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(dasher.dashCooldown);
        dasher.canDash = true;
    }
}

public interface ICanDash
{
    bool canDash { get; set; }

    float dashDistance { get; }
    float dashDuration { get; }
    float dashCooldown { get; }
}
