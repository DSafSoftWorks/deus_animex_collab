﻿using UnityEngine;

public class Attack : State
{
    private ICanAttack attacker;
    private float timer;
    private bool thrustActive;
    
    public Attack(Actor actor, ICanAttack attacker)
    {
        this.actor = actor;
        this.attacker = attacker;
        timer = attacker.attackDuration;
        thrustActive = (attacker.attackThrust > 0);
    }

    public override void Enter()
    {
        actor.animator.SetBool("Attacking", true);
        actor.rigidbody.velocity = actor.facingDirection.normalized * attacker.attackThrust;
    }

    public override void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= attacker.attackThrustDuration && thrustActive)
        {
            actor.rigidbody.velocity = Vector2.zero;
        }

        if (timer <= 0)
        {
            actor.currentState = null;
        }
    }

    public override void FixedUpdate()
    {}

    public override void Exit()
    {
        actor.animator.SetBool("Attacking", false);
    }
}

public interface ICanAttack
{
    float attackDuration { get; }
    float attackThrust { get; }
    float attackThrustDuration { get; }
}
