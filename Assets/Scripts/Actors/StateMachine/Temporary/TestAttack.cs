﻿using UnityEngine;

public class TestAttack : State
{
    private ICanTestAttack attacker;
    private float timer;
    private bool thrustActive;
    private string attackAnimationBool;

    private float offset = 0.3064516f;
    private Vector3 initPos;
    private Vector3 initRot;
    
    public TestAttack(Actor actor, ICanTestAttack attacker, string attackAnimationBool)
    {
        this.actor = actor;
        this.attacker = attacker;
        timer = attacker.attackDuration;
        thrustActive = (attacker.attackThrust > 0);
        this.attackAnimationBool = attackAnimationBool;
    }

    public override void Enter()
    {
        initPos = attacker.hitbox.transform.localPosition;
        initRot = attacker.hitbox.transform.localEulerAngles;

        attacker.hitbox.transform.localPosition = actor.facingDirection.normalized * offset;
        var tempRot = initRot;
        var tempDir = actor.facingDirection.normalized;
        if (tempDir.y != 0)
        {
            tempRot.z *= tempDir.y;
        }
        tempRot.z += 90 * tempDir.x;

        attacker.hitbox.transform.localEulerAngles = tempRot;

        attacker.hitbox.gameObject.SetActive(true);
        actor.animator.SetBool(attackAnimationBool, true);
        actor.rigidbody.velocity = actor.facingDirection.normalized * attacker.attackThrust;
    }

    public override void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= attacker.attackThrustDuration && thrustActive)
        {
            actor.rigidbody.velocity = Vector2.zero;
        }

        if (timer <= 0)
        {
            actor.currentState = null;
        }
    }

    public override void FixedUpdate()
    {}

    public override void Exit()
    {
        attacker.hitbox.gameObject.SetActive(false);
        actor.animator.SetBool(attackAnimationBool, false);

        attacker.hitbox.transform.localPosition = initPos;
        attacker.hitbox.transform.localEulerAngles = initRot;
    }
}

public interface ICanTestAttack
{
    float attackDuration { get; }
    float attackThrust { get; }
    float attackThrustDuration { get; }
    Collider2D hitbox { get; }
}
